# Crypto Info Web App
A **very** simple web application that allows a user to input some currency ticker IDs and gather information about those currencies.

This app is built using Ruby and the [Roda framework](https://roda.jeremyevans.net/).

### Small note / caveat
The API key that appeared on the tech test paper provided to me wasn't working and kept resulting in authentication errors. So to be able to do the tech test I ended up signing up for a free API key myself, this did however have one little snag in that the free API key I had was rate limited to 1 request per second, and so I needed to wrap some logic in a retry loop with a less than ideal `sleep` statement to get round this temporarily.

## Why a web app?
The tech test sheet specified to create an `application` to perform several tasks which is a bit ambiguous, a CLI app? a web app? a GUI app? So with this in mind, I decided to take the web application approach and just build something small and simple.

## Why Roda?
A simple web app like this doesn't require the heavy weight of Rails, and therefore I wanted to have a much ligher easy to get up and running framework. Roda would still be a choice for me for apps larger than this if a lot of the 'out of the box' rails functionality isn't required due to the massive difference in requests per second and memory usage between the two.

However in an app where fast prototyping and development is required within a team where Rails is the common standard that everyone is able to collaborate over that would always be an obvious front-runner.

## Usage
### Run the web application
Ensure you have `Docker` (with `Compose`) and `make` installed.

After cloning the repository run the following commands:

```
make build
make up
```

and navigate to `localhost:9292` for features / tasks 1, 2, and 3 of the tech test.  
Navigate to `localhost:9292/currencies/compare` for feature / task 4 of the tech test.

### Run the tests
```
make test
```

## Development

Docker Compose is used to build the local environment, this is my preferred method of working for multiple reasons, including not cluttering my local machine and more importantly when in team / onboarding situations, quick onboarding and a reduction of "But it works on my machine" syndrome.

A few make tasks have been provided to run certain tools in development:

- **`make build`** to build the project locally.
- **`make up`** to bring up the container for the app, making it accessible from `localhost:9292`.
- **`make down`** to tear down containers and network.
- **`make test`** to run the test suite.
- **`make document-behaviour`** to run the test suite in documentation mode to see the intended system behaviour.

## Tests defining the behaviour
Documented Behaviour output (after removing log noise) from a `make document-behaviour`:
```
/currencies
  GET
    should display a form that allows users to search for currencies
  POST
    with a valid list of ids input
      shows a table for each currency id
    with an empty list of ids input
      shows an error message prompting the user to input ids
    with no list of ids input
      shows an error message prompting the user to input ids
    with a list of empty ids input
      shows an error message prompting the user to input ids
    with a list of attributes to filter down to input
      only retrieves the attributes specified
    with a specified fiat conversion currency
      displays the prices for the specified fiat
  /compare
    GET
      should display a page with a form to compare currencies
    POST
      should show the resulting valuation

Currency
  can find and return a collection of currency objects
  is able to access response data like a standard ruby method/attribute
  is able to return a list of its attributes
  is able to filter the attributes when retrieving currencies
  can have the fiat conversion currency specified
  when two currencies have a matching fiat conversion
    can express itself in terms of another currency
  when two currencies have different fiats
    can not express itself in terms of another currency
```

## How would I improve it?
Given the time frame the work was done over, there are obviously a few cracks in places where things aren't as solid as they should be if this were something going into production.

For example, if I were to take this forward into a proper application I would want to do more exploratory testing with edge case scenarios and odd user input, as at the moment there isn't any logic to cover oddities and so solidfying against that so users don't see error pages would be an improvement.

Another would be splitting out the main app file's route tree structure into separate files to keep the code smaller, easier to read, and keep files encapsulating only the logic for a specific area, rather than all in the main app file as it is now.

There also quite likely endless refactorings and improvements to the code, structure, abstractions etc. that could be done, so I would like to do that, and would think that with a test suite that covers all the behaviour that this could be done with confidence that things wouldn't break along the way of refactoring.

Another would be to use a production API key rather than the development one in use, as this has a rate limit on it to 1 request per second. So I have had to wrap some of the logic in the API adapter to retry on failures, which makes the code look less clean, I would like to be able to remove that.