# frozen_string_literal: true

require "roda"
require_relative 'models/currency'
require_relative 'adapters/currency_source/api_adapter'
require_relative 'adapters/currency_source/test_adapter'

class CryptocurrencyInformationApp < Roda
  class NoIdsEntered < StandardError; end

  plugin :render

  adapter = CurrencySource::TestAdapter if ENV['rack_env'] == 'test'
  adapter ||= CurrencySource::ApiAdapter

  puts ENV['rack_env']
  puts adapter

  route do |r|
    r.root { r.redirect 'currencies' }

    r.on 'currencies' do
      r.on 'compare' do
        # GET /currencies/compare
        r.get do
          view 'currencies/compare'
        end

        # POST /currencies/compare
        r.post do
          currencies = Currency.find_by_ids([r.params['currency_one'], r.params['currency_two']],
                                            source_adapter: adapter,
                                            fiat: r.params['fiat'])
          @first_currency = currencies.find { |currency| currency.id == r.params['currency_one'] }
          @second_currency = currencies.find { |currency| currency.id == r.params['currency_two'] }

          @valuation = @first_currency.value_in(@second_currency)
          view 'currencies/compare'
        end

      end

      # GET /currencies
      r.get do
        view 'currencies/index'
      end

      # POST /currencies
      r.post do
        raise NoIdsEntered if r.params['ids'].nil?
        # remove all white space and split on the comma delimiter
        requested_ids = r.params['ids'].gsub(/\s+/, '')
                                       .split(',')
                                       .reject(&:empty?)

        raise NoIdsEntered if requested_ids.empty?

        filters = [] if r.params['attribute_filters'].nil?
        filters ||= r.params['attribute_filters'].gsub(/\s+/, '')
                                               .split(',')
                                               .reject(&:empty?)

        @currencies = Currency.find_by_ids(requested_ids,
                                           source_adapter: adapter,
                                           attribute_filters: filters,
                                           fiat: r.params['fiat'])
        view 'currencies/index'

      rescue NoIdsEntered => e
        @errors = []
        @errors << 'Please input at least one ID'
        view 'currencies/index'
      end
    end
  end
end
