FROM ruby:3.0.1
WORKDIR /usr/src/app
COPY Gemfile Gemfile.lock ./
RUN bundle install
COPY . .
CMD [ "bundle", "exec", "rackup", "--host", "0.0.0.0" ]
