# frozen_string_literal: true

require 'bigdecimal'

class Currency
  class MismatchingFiatsError < StandardError; end

  DEFAULT_CONVERSION_FIAT = "USD"

  def self.find_by_ids(ids,
                       source_adapter: CurrencySource::ApiAdapter,
                       attribute_filters: [],
                       fiat: 'USD')
    attribute_filters << 'name' unless attribute_filters.empty? || attribute_filters.include?('name')

    source_adapter.get(ids, attribute_filters: attribute_filters, fiat: fiat).collect do |response|
      new(fiat, response)
    end
  end

  attr_reader :fiat, :attributes

  def initialize(fiat, data)
    @fiat = fiat
    @data = data.transform_keys(&:to_sym)
    @attributes = data.keys.map(&:to_sym)
  end

  def value_in(other_currency)
    raise MismatchingFiatsError unless self.fiat == other_currency.fiat

    BigDecimal(self.price.dup) / BigDecimal(other_currency.price.dup)
  end

  private
  attr_reader :data

  # Around the code it would be nicer to be able to use the data returned
  # as attributes rather than accessing hashes, so the below method missing
  # implementation allows that. Hopefully easier to refactor if the class
  # matures in the future into a more "proper" domain object, for now though
  # this lightweight solution should do.
  def method_missing(method_name, *args)
    data[method_name.to_sym] || super
  end

  def respond_to_missing?(method_name, include_private = false)
    attributes.include?(method_name.to_sym) || super
  end
end
