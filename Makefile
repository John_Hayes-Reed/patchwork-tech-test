build:
	docker-compose build
up:
	docker-compose up
down:
	docker-compose down
test:
	docker-compose run --rm web rspec
document-behaviour:
	docker-compose run --rm web rspec -f doc