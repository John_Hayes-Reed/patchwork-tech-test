# frozen_string_literal: true

require 'spec_helper'
require 'nokogiri'

describe '/currencies' do
  #
  # Feature 1 - Display payload info for given cryptocurrencies
  #
  context 'GET' do
    it 'should display a form that allows users to search for currencies' do
      # Act
      get '/currencies'

      # Assert
      aggregate_failures do
        expect(last_response.status).to eq 200
        expect(last_response.body).to include 'input name="ids"'
      end
    end
  end

  context 'POST' do
    context 'with a valid list of ids input' do
      it 'shows a table for each currency id' do
        # Arrange
        params = { ids: "BTC, ETH" }

        # Act
        post '/currencies', params

        # Assert
        aggregate_failures do
          expect(last_response.status).to eq 200
          expect(last_response.body).to include 'Bitcoin</h3>'
          expect(last_response.body).to include 'price</th>'
        end
      end
    end

    context 'with an empty list of ids input' do
      it 'shows an error message prompting the user to input ids' do
        # Arrange
        params = { ids: "" }

        # Act
        post '/currencies', params

        # Assert
        aggregate_failures do
          expect(last_response.status).to eq 200
          expect(last_response.body).to include 'Please input at least one ID'
        end
      end
    end

    context 'with no list of ids input' do
      it 'shows an error message prompting the user to input ids' do
        # Arrange
        params = {}

        # Act
        post '/currencies', params

        # Assert
        aggregate_failures do
          expect(last_response.status).to eq 200
          expect(last_response.body).to include 'Please input at least one ID'
        end
      end
    end

    context 'with a list of empty ids input' do
      it 'shows an error message prompting the user to input ids' do
        # Arrange
        params = { ids: ",,,," }

        # Act
        post '/currencies', params

        # Assert
        aggregate_failures do
          expect(last_response.status).to eq 200
          expect(last_response.body).to include 'Please input at least one ID'
        end
      end
    end

    #
    # Feature 2 - filter list of attributes to retrieve and display
    #

    context 'with a list of attributes to filter down to input' do
      it 'only retrieves the attributes specified' do
        # Arrange
        params = {
          ids: "BTC, ETH",
          attribute_filters: "price, circulating_supply"
        }

        # Act
        post '/currencies', params

        # Assert
        aggregate_failures do
          expect(last_response.status).to eq 200
          expect(last_response.body).to include '<th>price</th>'
          expect(last_response.body).to include '<th>circulating_supply</th>'
          expect(last_response.body).not_to include '<th>logo</th>'
          expect(last_response.body).not_to include '<th>price_date</th>'
        end
      end
    end

    #
    # Feature 3 - Specify fiat conversion
    #

    context 'with a specified fiat conversion currency' do
      it 'displays the prices for the specified fiat' do
        # Arrange
        default_params = { ids: "BTC" }
        fiat_params = { ids: "BTC", fiat: "AUD" }

        # Act
        post '/currencies', default_params
        default_fiat_price_content = Nokogiri::HTML(last_response.body).css('td#BitcoinPrice')
        post '/currencies', fiat_params
        specified_fiat_price_content = Nokogiri::HTML(last_response.body).css('td#BitcoinPrice')

        # Assert
        expect(default_fiat_price_content.text).not_to eq specified_fiat_price_content.text
      end
    end
  end

  #
  # Feature 4 - Get value of a given currency in terms of another currency, through
  #             a chosen base fiat conversion
  #

  describe '/compare' do
    context 'GET' do
      it 'should display a page with a form to compare currencies' do
        # Act
        get '/currencies/compare'

        # Assert
        aggregate_failures do
          expect(last_response.status).to eq 200
          expect(last_response.body).to include 'input name="currency_one"'
          expect(last_response.body).to include 'input name="currency_two"'
          expect(last_response.body).to include 'input name="fiat"'
        end
      end
    end

    context 'POST' do
      it 'should show the resulting valuation' do
        # Arrange
        params = { currency_one: 'BTC', currency_two: 'ETH', fiat: 'GBP' }

        # Act
        post '/currencies/compare', params
        valuation = Nokogiri::HTML(last_response.body).css('strong#valuation').text

        # Assert
        aggregate_failures do
          expect(last_response.status).to eq 200
          expect(last_response.body).to include 'BTC is'
          expect(last_response.body).to include valuation
          expect(last_response.body).to include 'ETH (through GBP)'
        end
      end
    end
  end
end
