# frozen_string_literal: true

require 'spec_helper'
require_relative '../../../adapters/currency_source/test_adapter'

RSpec.describe Currency do
  #
  # Feature 1 - Display payload info for given cryptocurrencies
  #
  it 'can find and return a collection of currency objects' do
    # Arrange
    sample_ids = ['BTC', 'ETH']

    # Act
    currencies = Currency.find_by_ids(
      sample_ids,
      source_adapter: CurrencySource::TestAdapter
    )

    # Assert
    aggregate_failures do
      expect(currencies).not_to be_empty
      expect(currencies).to all(be_a(Currency))
    end
  end

  # Want to be able to reference the returned data like a ruby object, so
  # wrap it in a Currency object and provide access through method calls.
  # Perhaps easier to refactor if needed with less outside changes.
  it 'is able to access response data like a standard ruby method/attribute' do
    # Arrange
    sample_ids = ['BTC']

    # Act
    currencies = Currency.find_by_ids(
      sample_ids,
      source_adapter: CurrencySource::TestAdapter
    )

    # Assert
    aggregate_failures do
      expect(currencies.first.respond_to?(:price)).to be true
      expect(currencies.first.price).to eq '100.0'

      expect(currencies.first.respond_to?(:name)).to be true
      expect(currencies.first.name).to eq 'Bitcoin'

      expect(currencies.first.respond_to?(:foobar)).to eq false
      expect { currencies.first.foobar }.to raise_error NoMethodError
    end
  end

  it 'is able to return a list of its attributes' do
    # Arrange
    sample_ids = ['BTC']

    # Act
    currencies = Currency.find_by_ids(
      sample_ids,
      source_adapter: CurrencySource::TestAdapter
    )

    # Assert
    aggregate_failures do
      expect(currencies.first.attributes).not_to be_empty
      expect(currencies.first.attributes).to include :name, :price
    end
  end

  #
  # Feature 2 - filter list of attributes to retrieve and display
  #
  it 'is able to filter the attributes when retrieving currencies' do
    # Arrange
    sample_ids = ['BTC', 'XRP']
    attribute_filters = ['name', 'price', 'circulating_supply']

    # Act
    currencies = Currency.find_by_ids(
      sample_ids,
      attribute_filters: attribute_filters,
      source_adapter: CurrencySource::TestAdapter
    )

    # Assert
    aggregate_failures do
      expect(currencies.first).to respond_to :name
      expect(currencies.first.name).to eq 'Bitcoin'
      expect(currencies.first).to respond_to :price
      expect(currencies.first.price).to eq '100.0'
      expect(currencies.first).to respond_to :circulating_supply
      expect(currencies.first.circulating_supply).to eq '17758462'
      expect(currencies.first).not_to respond_to :price_date
      expect { currencies.first.price_date }.to raise_error NoMethodError

      expect(currencies.last).to respond_to :name
      expect(currencies.last.name).to eq 'XRP'
      expect(currencies.last).to respond_to :price
      expect(currencies.last.price).to eq '1.11147249'
      expect(currencies.last).to respond_to :circulating_supply
      expect(currencies.last.circulating_supply).to eq '46805773456'
      expect(currencies.last).not_to respond_to :price_date
      expect { currencies.last.price_date }.to raise_error NoMethodError
    end
  end

  #
  # Feature 3 - be able to specify the fiat currency
  #
  it 'can have the fiat conversion currency specified' do
    # Arrange
    sample_ids = ['BTC']
    attribute_filters = ['price']
    fiat_conversion_currency = 'AUD'

    # Act
    default_currencies = Currency.find_by_ids(
      sample_ids,
      attribute_filters: attribute_filters,
      source_adapter: CurrencySource::TestAdapter
    )
    specified_fiat_currencies = Currency.find_by_ids(
      sample_ids,
      attribute_filters: attribute_filters,
      fiat: fiat_conversion_currency,
      source_adapter: CurrencySource::TestAdapter
    )

    # Assert
    expect(default_currencies.first.price).not_to eq specified_fiat_currencies.first.price
  end

  #
  # Feature 4 - Get value of a given currency in terms of another currency, through
  #             a chosen base fiat conversion
  #
  context 'when two currencies have a matching fiat conversion' do
    fiat_conversion_currency = 'USD'

    it 'can express itself in terms of another currency' do
      # Arrange
      sample_ids = ['BTC', 'ETH']
      expected_bitcoin_stated_in_ethereum = 2.0
      expected_ethereum_stated_in_bitcoin = 0.5

      # Act
      currencies = Currency.find_by_ids(
        sample_ids,
        source_adapter: CurrencySource::TestAdapter,
        fiat: fiat_conversion_currency
      )
      bitcoin = currencies.find { |currency| currency.id == 'BTC' }
      ethereum = currencies.find { |currency| currency.id == 'ETH' }
      bitcoin_stated_in_etheruem = bitcoin.value_in(ethereum)
      ethereum_stated_in_bitcoin = ethereum.value_in(bitcoin)

      # Assert
      aggregate_failures do
        expect(bitcoin_stated_in_etheruem).to eq expected_bitcoin_stated_in_ethereum
        expect(ethereum_stated_in_bitcoin).to eq expected_ethereum_stated_in_bitcoin
      end
    end
  end

  context 'when two currencies have different fiats' do
    it 'can not express itself in terms of another currency' do
      # Arrange
      currency_one = Currency.new('USD', {})
      currency_two = Currency.new('AUD', {})

      # Act & Assert
      expect { currency_one.value_in(currency_two) }.to raise_error Currency::MismatchingFiatsError
    end
  end
end