# frozen_string_literal: true

require 'bigdecimal'
module CurrencySource
  class TestAdapter
    def self.get(ids, attribute_filters: [], fiat: nil)
      currencies = []
      ids.each do |id|
        # dup so the hashes in the sample responses aren't mutated and are left as a constant
        currencies << SAMPLE_RESPONSES.detect { |response| response[:id] == id }&.dup
      end
      currencies.compact

      currencies.map{ |currency| currency[:price] = (currency[:price].to_f/2).to_s } if fiat == 'AUD'

      return currencies if attribute_filters.empty?

      currencies.map do |currency|
        currency.select{ |key, _value| attribute_filters.include? key.to_s }
      end
    end

    SAMPLE_RESPONSES = [
      {
        "currency": "BTC",
        "id": "BTC",
        "status": "active",
        "price": "100.0",
        "price_date": "2019-06-14T00:00:00Z",
        "price_timestamp": "2019-06-14T12:35:00Z",
        "symbol": "BTC",
        "circulating_supply": "17758462",
        "max_supply": "21000000",
        "name": "Bitcoin",
        "logo_url": "https://s3.us-east-2.amazonaws.com/nomics-api/static/images/currencies/btc.svg",
        "market_cap": "150083247116.70",
        "market_cap_dominance": "0.4080",
        "transparent_market_cap": "150003247116.70",
        "num_exchanges": "357",
        "num_pairs": "42118",
        "num_pairs_unmapped": "4591",
        "first_candle": "2011-08-18T00:00:00Z",
        "first_trade": "2011-08-18T00:00:00Z",
        "first_order_book": "2017-01-06T00:00:00Z",
        "first_priced_at": "2017-08-18T18:22:19Z",
        "rank": "1",
        "rank_delta": "0",
        "high": "19404.81116899",
        "high_timestamp": "2017-12-16",
        "1d": {
          "price_change": "269.75208019",
          "price_change_pct": "0.03297053",
          "volume": "1110989572.04",
          "volume_change": "-24130098.49",
          "volume_change_pct": "-0.02",
          "market_cap_change": "4805518049.63",
          "market_cap_change_pct": "0.03",
          "transparent_market_cap_change": "4800518049.00",
          "transparent_market_cap_change_pct": "0.0430",
          "volume_transparency": [
            {
              "grade": "A",
              "volume": "2144455081.37",
              "volume_change": "-235524941.08",
              "volume_change_pct": "-0.10"
            },
            {
              "grade": "B",
              "volume": "15856762.85",
              "volume_change": "-6854329.88",
              "volume_change_pct": "-0.30"
            }
          ]
        }
      },
      {
        "currency": "XRP",
        "id": "XRP",
        "status": "active",
        "price": "1.11147249",
        "price_date": "2019-06-14T00:00:00Z",
        "price_timestamp": "2019-06-14T12:35:00Z",
        "symbol": "XRP",
        "circulating_supply": "46805773456",
        "max_supply": "21000000",
        "name": "XRP",
        "logo_url": "https://s3.us-east-2.amazonaws.com/nomics-api/static/images/currencies/btc.svg",
        "market_cap": "150083247116.70",
        "market_cap_dominance": "0.4080",
        "transparent_market_cap": "150003247116.70",
        "num_exchanges": "357",
        "num_pairs": "42118",
        "num_pairs_unmapped": "4591",
        "first_candle": "2011-08-18T00:00:00Z",
        "first_trade": "2011-08-18T00:00:00Z",
        "first_order_book": "2017-01-06T00:00:00Z",
        "first_priced_at": "2017-08-18T18:22:19Z",
        "rank": "1",
        "rank_delta": "0",
        "high": "19404.81116899",
        "high_timestamp": "2017-12-16",
        "1d": {
          "price_change": "269.75208019",
          "price_change_pct": "0.03297053",
          "volume": "1110989572.04",
          "volume_change": "-24130098.49",
          "volume_change_pct": "-0.02",
          "market_cap_change": "4805518049.63",
          "market_cap_change_pct": "0.03",
          "transparent_market_cap_change": "4800518049.00",
          "transparent_market_cap_change_pct": "0.0430",
          "volume_transparency": [
            {
              "grade": "A",
              "volume": "2144455081.37",
              "volume_change": "-235524941.08",
              "volume_change_pct": "-0.10"
            },
            {
              "grade": "B",
              "volume": "15856762.85",
              "volume_change": "-6854329.88",
              "volume_change_pct": "-0.30"
            }
          ]
        }
      },
      {
        "currency": "ETH",
        "id": "ETH",
        "status": "active",
        "price": "50.0",
        "price_date": "2019-06-14T00:00:00Z",
        "price_timestamp": "2019-06-14T12:35:00Z",
        "symbol": "ETH",
        "circulating_supply": "17758462",
        "max_supply": "21000000",
        "name": "Ethereum",
        "logo_url": "https://s3.us-east-2.amazonaws.com/nomics-api/static/images/currencies/btc.svg",
        "market_cap": "150083247116.70",
        "market_cap_dominance": "0.4080",
        "transparent_market_cap": "150003247116.70",
        "num_exchanges": "357",
        "num_pairs": "42118",
        "num_pairs_unmapped": "4591",
        "first_candle": "2011-08-18T00:00:00Z",
        "first_trade": "2011-08-18T00:00:00Z",
        "first_order_book": "2017-01-06T00:00:00Z",
        "first_priced_at": "2017-08-18T18:22:19Z",
        "rank": "1",
        "rank_delta": "0",
        "high": "19404.81116899",
        "high_timestamp": "2017-12-16",
        "1d": {
          "price_change": "269.75208019",
          "price_change_pct": "0.03297053",
          "volume": "1110989572.04",
          "volume_change": "-24130098.49",
          "volume_change_pct": "-0.02",
          "market_cap_change": "4805518049.63",
          "market_cap_change_pct": "0.03",
          "transparent_market_cap_change": "4800518049.00",
          "transparent_market_cap_change_pct": "0.0430",
          "volume_transparency": [
            {
              "grade": "A",
              "volume": "2144455081.37",
              "volume_change": "-235524941.08",
              "volume_change_pct": "-0.10"
            },
            {
              "grade": "B",
              "volume": "15856762.85",
              "volume_change": "-6854329.88",
              "volume_change_pct": "-0.30"
            }
          ]
        }
      }
    ].freeze
  end
end
