# frozen_string_literal: true

require 'net/http'
require 'json'

module CurrencySource
  class ApiAdapter
    class RateLimitError < StandardError; end

    API_KEY = "fe2c3e29f705b6ba7d57198490da3b21ab0e9d8f"
    API_URI = "https://api.nomics.com/v1/currencies/ticker?key=#{API_KEY}"

    def self.get(ids, attribute_filters: [], fiat:)
      ids_param = "&ids=#{ids.join(',')}"
      fiat_param = "&convert=#{fiat}"
      uri = URI("#{API_URI}#{ids_param}#{fiat_param}")

      # API_KEY in use is a non-paid for API key, and therefore the rate limit
      # is 1 request per second. Wrapping in a retry loop that waits for a
      # little over that limit to avoid potential errors. Obviously this
      # would not be needed if using a paid for API key.
      begin
        attempts ||= 1
        response = Net::HTTP.get(uri)

        parsed_response = JSON.parse(response)

        return parsed_response if attribute_filters.empty?

        parsed_response.each do |currency|
          currency.select! { |key, _value| attribute_filters.include?(key) }
        end

      rescue JSON::ParserError => e
        if (attempts += 1) <= 3
          # Rate limit for non-paid API Key is 1 request per second
          sleep 1.2
          retry
        else
          raise
        end
      end
    end
  end
end
